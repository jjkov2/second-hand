from django.urls import path
from .views import *

urlpatterns = [
    path("", index, name="home"),
    path("registration/", create_user, name="registration"),
    path("login/", auth_user, name="auth_user"),
    path("searched_query", searched_query, name="searched_query"),
    path("create_ad/", create_ad, name="create_ad"),
    path("product/<int:products_id>/", view_products, name="view_products"),
    path("user/<int:users_id>/", view_users, name="view_users"),
    path("user/<int:user_id>/saled/<int:saled>/", view_user_ad, name="view_user_ad"),
]