from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.views.decorators.csrf import csrf_protect

from .forms import *
from .models import *

search_query_result = None

def search(query):
    result = []
    for i in Category.objects.all():
        if f"{(query.lower())}" in str(i.title).lower():
            result.append(i)
    for i in Product.objects.all():
        if query.lower() in (i.title).lower():
            result.append(i)
    return result            

user = None
avatar = None

def get_user_as_object(request):
    global user, avatar
    if request.user in CustomUser.objects.all():
        user = request.user
        avatar = user.photo
    else:
        user = None
        avatar = None
    print(user)

def index(request):
    global search_query_result, user, avatar
    get_user_as_object(request)

    photo = ProductPhoto.objects.all()
    if request.method == "POST":
        form = Search(request.POST)
        search_query_result = search(request.POST["search_field"])
        return redirect("searched_query")
    if request.method == "GET":
        form = Search()
        
    products = Product.objects.all()
    context = {"products": products, "form": form, "photo": photo, "avatar": avatar, "user": user}
    return render(request, "marketPlace/index.html", context)

@csrf_protect
def create_user(request):
    global user, avatar
    get_user_as_object(request)

    if request.method == "POST":
        form = CustomUserCreationForm(request.POST, request.FILES)
        user = CustomUser.objects.create_user(email=request.POST["email"], password=request.POST["password1"], username=request.POST["username"])
        user.photo = request.FILES["photo"]
        user.save()
        return redirect("home")
    else:
        form = CustomUserCreationForm()
    return render(request, "marketPlace/create_user.html", {"form": form, "user": user, "avatar": avatar})

def auth_user(request):
    global user, avatar
    get_user_as_object(request)

    if request.method == "POST":
            form = AuthUser(request.POST)
            email = request.POST['email']
            password = request.POST['password']
            user = authenticate(request, email=email, password=password)
            if user is not None:
                login(request, user)
                return redirect("home")
    else:
        form = AuthUser()
    return render(request, "marketPlace/login_user.html", {"form": form, "user": user, "avatar": avatar})

def searched_query(request):
    global search_query_result
    global user, avatar
    get_user_as_object(request)

    category = Category.objects.all()
    if search_query_result[0] in category:
        products = Product.objects.filter(category_id=search_query_result[0].id)
    else:
        products = search_query_result
    return render(request, "marketPlace/searched_query.html", {"products": products, "user": user, "avatar": avatar})

def create_ad(request):
    global user, avatar
    get_user_as_object(request)

    if request.method == "POST":
        form_create = CreateAdForm(request.POST)
        photo = ProductPhoto(photo=request.FILES["photo"])
        photo.save()
        category = Category.objects.get(id=request.POST["category"])
        ad = Product(title=request.POST["title"], category=category, description=request.POST["description"], price=request.POST["price"], salesman=request.user, adress=request.POST["adress"], photo=photo)
        ad.save()
        return redirect("home")
    else:
        form_create = CreateAdForm()
        form_add_photo = ProductPhoto()
    context = {"form_create": form_create, "user": user, "avatar": avatar}
    return render(request, "marketPlace/create_product.html", context)

def view_products(request, products_id):
    global user, avatar
    get_user_as_object(request)
    
    products_item = Product.objects.get(pk=products_id)
    return render(request, "marketPlace/view_product.html", {"product": products_item, "user": user, "avatar": avatar})

def view_users(request, users_id):
    user_item = CustomUser.objects.get(pk = users_id)
    return render(request, "marketPlace/view_user.html", {"user_item": user_item, "user": user, "avatar": avatar})

def view_user_ad(request, user_id, saled):
    user_item = CustomUser.objects.get(id=user_id)
    if saled ==  1:
        products = Product.objects.filter(sales=True)
    elif saled ==  0:
        products = Product.objects.filter(sales=False)
    return render(request, "marketPlace/view_user_products.html", {"user_item": user_item, "products": products})
