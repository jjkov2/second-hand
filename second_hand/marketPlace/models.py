from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.urls import reverse

from .managers import CustomUserManager

class CustomUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(_('email address'), unique=True)
    username = models.CharField(max_length=150, verbose_name="Имя пользователя")
    is_staff = models.BooleanField(default=False, verbose_name="Работник")
    is_active = models.BooleanField(default=True, verbose_name="Активен")
    date_joined = models.DateTimeField(default=timezone.now, verbose_name="Дата регистрации")
    photo = models.ImageField(upload_to="media/avatars/%Y%M%d", verbose_name="Аватар пользователя")
    
    USERNAME_FIELD = 'email'

    REQUIRED_FIELDS = ["username" , "password"]

    objects = CustomUserManager()

    class Meta:
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"

    def __str__(self):
        return self.email

    def get_absolute_url(self):
        return reverse("view_users", kwargs={"users_id": self.pk})
    
    def get_products_wich_saling(self):
        return reverse("view_user_ad", kwargs={"user_id": self.pk, "saled": 0})
    
    def get_products_wich_saled(self):
        return reverse("view_user_ad", kwargs={"user_id": self.pk, "saled": 1})


class Product(models.Model):
    title = models.CharField(max_length=100, verbose_name="Название")
    category = models.ForeignKey("Category", verbose_name=_("Категория"), on_delete=models.CASCADE)
    description = models.TextField(verbose_name="Описание")
    price = models.IntegerField(verbose_name="Цена")
    adress = models.CharField(max_length=150, verbose_name="Адрес")
    salesman = models.ForeignKey(CustomUser, verbose_name=_("Продавец"), on_delete=models.CASCADE)
    photo = models.ForeignKey("ProductPhoto", verbose_name="Фотография товара", on_delete=models.CASCADE, null=True)
    time_created = models.DateTimeField(auto_now_add=True, verbose_name="Дата публикации")
    sales = models.BooleanField(default=False ,verbose_name="Продано?")

    class Meta:
        verbose_name = "Товар"
        verbose_name_plural = "Товары"
        ordering = ["time_created"]

    REQUIRED_FIELDS = ["title", "category", "description", "price"]
    
    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("view_products", kwargs={"products_id": self.pk})
    
    

class Category(models.Model):
    title = models.CharField(max_length=50)

    class Meta:
        verbose_name = "Категория"
        verbose_name_plural = "Категории"

    def __str__(self):
        return self.title

class ProductPhoto(models.Model):
    photo = models.ImageField(upload_to="media/product_photo/%Y%M%d", verbose_name="Фотография")
    class Meta:
        verbose_name = "Фотография товара"
        verbose_name_plural = "Фотографии товара"

class Messanger(models.Model):
    pass

class Message(models.Model):
    pass