from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from .models import *


class CustomUserCreationForm(UserCreationForm):

    class Meta:
        model = CustomUser
        fields = ("email", "username", "photo")


class CustomUserChangeForm(UserChangeForm):

    class Meta:
        model = CustomUser
        fields = ('email', "password")

class AuthUser(forms.ModelForm):
    class Meta:
        model = CustomUser
        fields = ("email", "password")

class Search(forms.Form):
    search_field = forms.CharField(max_length=200, label="Поиск")

class CreateAdForm(forms.ModelForm):
    class Meta:
        model = Product
        fields  = ("__all__")

class AddPhotoOfProductForm(forms.ModelForm):
    class Meta:
        model = ProductPhoto
        fields = ("photo", )
        widget = {
            "photo": forms.FileInput(attrs={"class": forms.FileInput}),
        }